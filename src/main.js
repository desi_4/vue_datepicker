import Vue from 'vue'
import App from './App.vue'

import 'bulma/bulma.sass'
import { library } from '@fortawesome/fontawesome-svg-core'
import { 
  faCalendar, 
  faMousePointer, 
  faChevronCircleLeft, 
  faChevronCircleRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '@fortawesome/vue-fontawesome'

library.add(faCalendar, faMousePointer, faChevronCircleLeft, faChevronCircleRight)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('font-awesome-layers', FontAwesomeLayers);
Vue.component('font-awesome-layers-text', FontAwesomeLayersText);

Vue.config.productionTip = false

export const EventBus = new Vue()

new Vue({
  render: h => h(App)
}).$mount('#app')
